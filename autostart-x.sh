# From: https://wiki.archlinux.org/title/Xinit#Autostart_X_at_login
# Add the following to your shell initialization:
#    . <path/to/autostart-x.sh>

if [ -z "$DISPLAY" ] && [ "$XDG_VTNR" = 1 ]; then
  exec startx
fi
