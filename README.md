# RPi Photo Display

This is a minimal set of configuration files for setting up a Raspberry Pi as photo
display with a clock on it. To use it, you need a Raspberry Pi with the following
software installed on it:

0. X11 (I haven't tried Wayland yet) (and associated tools like `xrandr`,
   `xdotool`)
1. The i3 window manager (+ picom for compositing)
2. Conky
3. The feh image viewer
4. Fonts: Ubuntu Mono (for English), Noto CJK (for Japanese)

The included `gett@tty1` override autologins a user (in this case, `muru`'s my
username), and my user's shell config is set [to start X11 on
TTY1][autostart-x11]. Then the included `xinitrc`:

1. Turns of X11 display power management to prevent the screen from blanking,
2. Then starts i3.

And the i3 config in turn starts:

1. picom for compositing, as Conky's transparency requires a compositor,
2. A script for switching Conky between English and Japanese date/time
   displays, and
3. A script which loops over images in a directory and periodically switches
   the background image using feh.
4. `xdotool` to move the mouse to a corner.

The sizes in the Conky config are for a <strike>1280x720</strike> 1920x1080
resolution 7" display (Arch Linux ARM could only get 720p, but Ubuntu 22.04
somehow gets 1080p). The main reason for alternating two Conkys is to prevent
burn-in (the displayed text is just across each instance just enough that I
think significant burn-in is unlikely). The best option would have been to
alternate the two displays on the left and right sides, but this works well
enough.

## Suggested file locations

Link (or copy) the files in this repo to these locations:

| Repo file                  | Suggested location                                       |
|----------------------------|----------------------------------------------------------|
| `switch-conky`             | `~/bin/switch-conky`                                     |
| `set-wallpaper`            | `~/bin/set-wallpaper`                                    |
| `set-wallpaper.conf`       | `~/.config/set-wallpaper.conf`                           |
| `conky-*.conf`             | `~/.config/conky/conky-*.conf`                           |
| `i3-config`                | `~/.config/i3/config`                                    |
| `getty@tty1-override.conf` | `/etc/systemd/system/getty@tty1.service.d/override.conf` |
| `autostart-x.sh`           | Anywhere (source it from your shell's rc files)          |
| `cloud-init.yaml`          | Include in your cloud-init userdata.                     |

If you have a custom `$XDG_CONFIG_HOME`, use that instead of `~/.config`.

[autostart-x11]: https://wiki.archlinux.org/title/Xinit#Autostart_X_at_login
